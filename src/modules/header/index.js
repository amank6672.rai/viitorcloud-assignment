import { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";

const Header = () => {
  const { dispatch, state } = useContext(AuthContext);
  const handleLogout = () => {
    dispatch({
      type: "LOGOUT",
    });
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/dashboard">
        App
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/dashboard">
              Dashboard
            </Link>
          </li>
          <li className="nav-item active">
            <Link className="nav-link" to="/about-us">
              About Us
            </Link>
          </li>
        </ul>
        <form className="form-inline my-2 my-lg-0">
          <span className="px-3">{state?.username}</span>
          <button
            className="btn btn-outline-success my-2 my-sm-0"
            type="button"
            onClick={handleLogout}
          >
            Logout
          </button>
        </form>
      </div>
    </nav>
  );
};

export default Header;
