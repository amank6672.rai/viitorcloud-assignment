import { useContext, useState } from "react";
import { AuthContext } from "../../context/AuthContext";
import "./Login.css";

const Login = () => {
  const [user, setUser] = useState({
    username: "",
    password: "",
  });
  const { dispatch } = useContext(AuthContext);

  const handleChange = (e) => {
    let tempUser = { ...user, [e.target.name]: e.target.value };
    setUser(tempUser);
  };

  const submitForm = () => {
    let credentials = {
      username: user.username,
      userId: 123,
      isAuthenticated: true,
    };
    dispatch({
      type: "LOGIN",
      payload: credentials,
    });
  };

  return (
    <div className="wrapper fadeInDown">
      <div id="formContent">
        <div className="fadeIn first">
          <img
            src="https://img.icons8.com/bubbles/2x/user-male.png"
            id="icon"
            alt="User Icon"
          />
        </div>
        <form>
          <div className="form-group">
            <label htmlFor="username">User Name</label>
            <input
              type="text"
              className="form-control"
              name="username"
              onChange={handleChange}
              value={user.username}
              placeholder="Enter username"
              id="username"
            />
          </div>
          <div className="form-group">
            <label htmlFor="pwd">Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter password"
              name="password"
              onChange={handleChange}
              value={user.password}
              id="pwd"
            />
          </div>

          <button
            type="button"
            className="btn btn-primary"
            onClick={submitForm}
            disabled={user.username === "" || user.password === ""}
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
