import React, { Suspense, lazy, useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";

const Auth = lazy(() => import("../modules/auth"));
const Dashboard = lazy(() => import("../modules/dashboard"));
const AboutUs = lazy(() => import("../modules/about-us"));

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { state } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={(props) =>
        state && state.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/auth/signin",
            }}
          />
        )
      }
    />
  );
};

const PublicRoute = ({ component: Component, ...rest }) => {
  const { state } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={(props) =>
        state && state.isAuthenticated ? (
          <Redirect
            to={{
              pathname: "/dashboard",
            }}
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

function AppRouter() {
  return (
    <section>
      <Suspense
        fallback={<div className="page-loading-section">Loading...</div>}
      >
        <Switch>
          <PublicRoute path="/auth" component={Auth} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <PrivateRoute path="/about-us" component={AboutUs} />
          <Route
            path="/"
            render={() => (
              <Redirect
                to={{
                  pathname: "/dashboard",
                }}
              />
            )}
          />
        </Switch>
      </Suspense>
    </section>
  );
}

export default AppRouter;
