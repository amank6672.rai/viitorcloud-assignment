import { Suspense } from "react";
import { BrowserRouter } from "react-router-dom";
import AuthProvider from "../context/AuthContext";
import "./App.css";
import AppRouter from "./AppRoute";

function App() {
  return (
    <Suspense fallback="loading">
      <BrowserRouter>
        <AuthProvider>
          <AppRouter></AppRouter>
        </AuthProvider>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
