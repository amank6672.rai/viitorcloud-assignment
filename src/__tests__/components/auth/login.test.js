import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import Login from "../../../modules/auth";
import AuthProvider from "../../../context/AuthContext";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

describe("Auth Login Form", () => {
  afterEach(cleanup);

  it("submit button disable", () => {
    const history = createMemoryHistory();
    const { getByText } = render(
      <Router history={history}>
        <AuthProvider>
          <Login />
        </AuthProvider>
      </Router>
    );
    expect(getByText(/Submit/i).closest("button")).toHaveAttribute("disabled");
  });

  it("submit button enable", async () => {
    const history = createMemoryHistory();
    const { getByText, findByLabelText } = render(
      <Router history={history}>
        <AuthProvider>
          <Login />
        </AuthProvider>
      </Router>
    );
    const username = await findByLabelText("User Name");
    fireEvent.change(username, { target: { value: "Aman" } });
    const password = await findByLabelText("Password");
    fireEvent.change(password, { target: { value: "test@123" } });
    expect(getByText(/Submit/i).closest("button")).not.toBeDisabled();
  });

  it("Submit the form", async () => {
    const history = createMemoryHistory();
    const { getByText, findByText, findByLabelText } = render(
      <Router history={history}>
        <AuthProvider>
          <Login />
        </AuthProvider>
      </Router>
    );
    const username = await findByLabelText("User Name");
    fireEvent.change(username, { target: { value: "Aman" } });
    const password = await findByLabelText("Password");
    fireEvent.change(password, { target: { value: "test@123" } });
    fireEvent.click(await findByText("Submit"));
    expect(getByText("App")).toBeInTheDocument();
  });
});
