import React from "react";
import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import AboutUs from "../../../modules/about-us";

describe("Dashboard", () => {
  afterEach(cleanup);
  it("check if component is rendered", async () => {
    const { findByText } = render(<AboutUs />);
    const aboutUs = await findByText("About Us");
    expect(aboutUs).toBeInTheDocument();
  });
});
