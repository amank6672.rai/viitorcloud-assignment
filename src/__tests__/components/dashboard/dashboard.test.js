import React from "react";
import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import Dashboard from "../../../modules/dashboard";

describe("Dashboard", () => {
  afterEach(cleanup);
  it("check if component is rendered", async () => {
    const { findByText } = render(<Dashboard />);
    const dashboard = await findByText("Dashboard");
    expect(dashboard).toBeInTheDocument();
  });
});
