import React from "react";
import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import Header from "../../../modules/header";
import AuthProvider from "../../../context/AuthContext";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";

describe("Header", () => {
  afterEach(cleanup);
  it("check if component is rendered", async () => {
    const history = createMemoryHistory();

    const { findByText } = render(
      <Router history={history}>
        <AuthProvider>
          <Header />
        </AuthProvider>
      </Router>
    );
    const headerMenuItem = await findByText("App");
    expect(headerMenuItem).toBeInTheDocument();
  });
});
