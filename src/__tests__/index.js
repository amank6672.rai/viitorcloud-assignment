import { render, screen } from "@testing-library/react";
import App from "../app/App";

test("Renders App", () => {
  render(<App />);
  //Loading from suspense
  const linkElement = screen.getByText(/loading/i);
  expect(linkElement).toBeInTheDocument();
});
