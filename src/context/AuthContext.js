import { createContext, useEffect, useReducer } from "react";
import { useHistory } from "react-router-dom";
import Header from "../modules/header";

export const AuthContext = createContext();
const localStorageState = localStorage.getItem("user");

let initialState = {};
try {
  initialState = localStorageState
    ? JSON.parse(localStorageState)
    : {
        isAuthenticated: false,
        user: null,
      };
} catch (err) {
  initialState = {
    isAuthenticated: false,
    user: null,
  };
}

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.payload));
      return {
        ...state,
        isAuthenticated: true,
        user: { ...action.payload },
      };
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    default:
      return state;
  }
};
function AuthProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const history = useHistory();

  useEffect(() => {
    if (
      !state?.isAuthenticated &&
      !window.location.pathname.includes("/auth")
    ) {
      history.push("/auth/sigin");
    }
  }, [history]);

  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      <>
        {state?.isAuthenticated ? (
          <>
            <Header />
            {props.children}
          </>
        ) : (
          <>{props.children}</>
        )}
      </>
    </AuthContext.Provider>
  );
}
export default AuthProvider;
